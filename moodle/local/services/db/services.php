<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service Template
 *
 * @package    services
 * Developer: 2020 Ricoshae Pty Ltd (http://ricoshae.com.au)
 */

// We defined the web service functions to install.
$functions = array(
    'local_services_create_quiz' => array(
        'classname'   => 'local_services_external',
        'methodname'  => 'create_quiz',
        'classpath'   => 'local/services/externallib.php',
        'description' => 'Create Quiz',
        'type'        => 'write',
        'ajax' => true,
        'capabilities' => '',
        'loginrequired' => true
    ),
    'local_services_create_assignment' => array(
        'classname'   => 'local_services_external',
        'methodname'  => 'create_assignment',
        'classpath'   => 'local/services/externallib.php',
        'description' => 'Create Assignment',
        'type'        => 'write',
        'ajax' => true,
        'capabilities' => '',
        'loginrequired' => true
    ),
    'local_services_create_folder' => array(
        'classname'   => 'local_services_external',
        'methodname'  => 'create_folder',
        'classpath'   => 'local/services/externallib.php',
        'description' => 'Create Folder',
        'type'        => 'write',
        'ajax' => true,
        'capabilities' => '',
        'loginrequired' => true
    ),
    'local_services_create_file' => array(
        'classname'   => 'local_services_external',
        'methodname'  => 'create_file',
        'classpath'   => 'local/services/externallib.php',
        'description' => 'Create File',
        'type'        => 'write',
        'ajax' => true,
        'capabilities' => '',
        'loginrequired' => true
    ),
    'local_services_create_label' => array(
        'classname'   => 'local_services_external',
        'methodname'  => 'create_label',
        'classpath'   => 'local/services/externallib.php',
        'description' => 'Create Label',
        'type'        => 'write',
        'ajax' => true,
        'capabilities' => '',
        'loginrequired' => true
    ),
    'local_services_create_url' => array(
        'classname'   => 'local_services_external',
        'methodname'  => 'create_url',
        'classpath'   => 'local/services/externallib.php',
        'description' => 'Create URL',
        'type'        => 'write',
        'ajax' => true,
        'capabilities' => '',
        'loginrequired' => true
    ),
    'local_services_get_students_in_course' => array(
        'classname'   => 'local_services_external',
        'methodname'  => 'get_students_in_course',
        'classpath'   => 'local/services/externallib.php',
        'description' => 'Fetch Students in a Course',
        'type'        => 'write',
        'ajax' => true,
        'capabilities' => '',
        'loginrequired' => true
    ),
    'local_services_upload' => array(
        'classname'   => 'local_services_external',
        'methodname'  => 'upload',
        'classpath'   => 'local/services/externallib.php',
        'description' => 'Upload file',
        'type'        => 'write',
        'ajax' => true,
        'capabilities' => '',
        'loginrequired' => true
    ),
);
