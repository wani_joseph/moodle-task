<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service Template
 *
 * @package    Externallib
 * Developer: 2020 Ricoshae Pty Ltd (http://ricoshae.com.au)
 */

require_once($CFG->libdir . "/externallib.php");
require_once($CFG->dirroot . "/local/services/lib.php");

class local_services_external extends external_api
{
    /**
     * Returns description of create_quiz parameters
     * @return external_function_parameters
     */
    public static function create_quiz_parameters()
    {
        $quizfields = [
            'name' => new external_value(PARAM_TEXT, 'The Quiz name'),
            'description' => new external_value(PARAM_TEXT, 'The Quiz description'),
            'openquizenabled' => new external_value(PARAM_BOOL, 'Quiz open time'),
            'timeopen' => new external_value(PARAM_BOOL, 'Quiz open time', VALUE_OPTIONAL),
            'closequizenabled' => new external_value(PARAM_BOOL, 'Quiz close time'),
            'timeclose' => new external_value(PARAM_BOOL, 'Quiz close time', VALUE_OPTIONAL),
            'quiztimingenabled' => new external_value(PARAM_BOOL, 'Quiz limit time'),
            'timelimit' => new external_value(PARAM_BOOL, 'Quiz limit time', VALUE_OPTIONAL),
            'graded' => new external_value(PARAM_BOOL, 'whether the quiz is graded or not'),
            'maxgrade' => new external_value(PARAM_TEXT, 'Max grade value if the quiz graded', VALUE_OPTIONAL),
            'announcments' => new external_value(PARAM_BOOL, 'The Quiz description'),
            'topic' => new external_value(PARAM_INT, 'If course format is topics, then topic id or section id', VALUE_OPTIONAL),
            'selecteddate' => new external_value(PARAM_TEXT, 'If course format weeks, which topic to insert the course in', VALUE_OPTIONAL),
            'postonedmodo' => new external_value(PARAM_BOOL, 'If enable post the quiz on edmodo lms'),
            'courses' => new external_multiple_structure(
                new external_value(PARAM_INT, 'Course ID')
            )
        ];

        return new external_function_parameters(
            [
                'quizzes' => new external_multiple_structure(
                    new external_single_structure($quizfields)
                )
            ]
        );
    }

    public static function create_quiz($quizzes)
    {
        global $DB, $USER;

        $usercourses = Get_Courses_User_Teaching($USER->id);
        $result = "";
        $quizes = "";
        $quizandmoduleid = array();

        $params = self::validate_parameters(self::create_quiz_parameters(), array('quizzes' => $quizzes));

        foreach ($params['quizzes'] as $quiz) {

            foreach (array('name', 'description') as $fieldname) {
                if (trim($quiz[$fieldname]) === '') {
                    throw new invalid_parameter_exception('The field ' . $fieldname . ' cannot be blank');
                }
            }

            $grade = 0;
            $openquiz = 0;
            $closequiz = 0;
            $quiztiming = 0;

            $coursesarray = array();

            $name = $quiz['name'];
            $desc = $quiz['description'];

            $topic = $quiz['topic'];
            $sectionel = $quiz['section'];
            $selectedDate = $quiz['selecteddate'];
            $announcments = $quiz['announcments'];

            $selectedDate = new DateTime();
            $selectedDate->setTimestamp($quiz['selecteddate']);

            $weekSelected = null;
            $topicSelected = null;
            $sectionSelected = null;

            $groupId = null;

            if ($quiz['graded'] == 1) {
                $grade = $quiz['maxgrade'];
            }

            if ($quiz['openquizenabled'] == 1) {
                $openquiz = $quiz['timeopen'];
            }

            if ($quiz['closequizenabled'] == 1) {
                $closequiz = $quiz['timeclose'];
            }

            if ($quiz['quiztimingenabled'] == 1) {
                $quiztiming = $quiz['timelimit'];
            }

            foreach ($usercourses as $course) {

                if (in_array($course->cid, $quiz['courses'])) {
                    $crsid = $course->cid;
                    array_push($coursesarray, $crsid);

                    if ($announcments && $announcments > 0) {
                        $weekSelected = null;
                        $topicSelected = null;
                    } else if ($course->format == 'topics' && $topic > 0) {
                        $topics = array();
                        $topics[0] = "Select Topic";
                        $i = 1;
                        $courseSections = $DB->get_records('course_sections', array('course' => $course->cid));

                        foreach ($courseSections as $section) {
                            if ($section->section > 0) {
                                $topics[$i] = $section->name != null ? $section->name : ('Topic ' . $section->section);
                                if ($i == $topic) {
                                    $topicSelected = $section->section;
                                    break;
                                }
                                $i++;
                            }
                        }
                    } else if ($course->format == 'flexsections' && $sectionel > 0) {
                        $sections = array();
                        $sections[0] = "Select Section";
                        $i = 1;
                        $courseSections = $DB->get_records('course_sections', array('course' => $course->cid));

                        foreach ($courseSections as $section) {
                            if ($section->section > 0) {
                                $sections[$i] = $section->name != null ? $section->name : ('Section ' . $section->section);
                                if ($i == $sectionel) {
                                    $sectionSelected = $section->section;
                                    break;
                                }
                                $i++;
                            }
                        }
                    } else if ($course->format == 'weeks') {
                        $startDate = new DateTime();
                        $startDate->setTimestamp($course->startdate);

                        $endDate = new DateTime();
                        $endDate->setTimestamp($course->enddate);

                        $interval = new DateInterval('P1D');
                        $dateRange = new DatePeriod($startDate, $interval, $endDate);
                        $weekNumber = 1;
                        $start = "";
                        $end = "";
                        $weeks = array();
                        $countDays = 1;

                        foreach ($dateRange as $date) {
                            $weeks[$weekNumber][] = $date;
                            $length = sizeof($weeks['' . $weekNumber . '']);

                            if ($length == 1) {
                                $start = $date;
                            }

                            if ($countDays == 7) {
                                $end = $date;

                                if ($selectedDate >= $start && $selectedDate <= $end) {
                                    $weekSelected = $weekNumber;
                                    break;
                                }

                                $weekNumber++;
                                $countDays = 1;
                            } else {
                                $countDays++;
                            }
                        }
                    } else {
                        continue;
                    }

                    if ($topicSelected && $course->format !== 'topics') {
                        continue;
                    }

                    if ($weekSelected && $course->format !== 'weeks') {
                        continue;
                    }

                    // $selectedStudents = $_POST["course-" . $crsid . "-students"];

                    // if ($selectedStudents) {
                    //     $groupId = createAndAssignGroupToCourse($crsid, $selectedStudents);
                    // } else {
                    //     $groupId = null;
                    // }

                    $result = Save_Quiz($crsid, $name, $desc, $grade, $openquiz, $closequiz, $quiztiming, $topicSelected, $sectionSelected, $weekSelected, $groupId);
                    $quizandmoduleid = explode(',', $result);
                    $quizes .= $quizandmoduleid[0] . ",";
                }
            }
        }

        return json_encode($quizes);
    }

    /**
     * Returns description of create_quiz result value
     * @return external_description
     */
    public static function create_quiz_returns()
    {
        return new external_value(PARAM_TEXT, 'Quizzies ids seperated using comma');
    }

    /**
     * Returns description of create_assignment parameters
     * @return external_function_parameters
     */
    public static function create_assignment_parameters()
    {
        $quizfields = [
            'name' => new external_value(PARAM_TEXT, 'The Quiz name'),
            'description' => new external_value(PARAM_TEXT, 'The Quiz description'),
            'attachments' => new external_value(PARAM_BOOL, 'Quiz limit time', VALUE_OPTIONAL),
            'submissionfrom' => new external_value(PARAM_TEXT, 'Submission from date'),
            'due' => new external_value(PARAM_TEXT, 'Submission due date', VALUE_OPTIONAL),
            'showdescription' => new external_value(PARAM_BOOL, 'If enabled show description in course page'),
            'onlinetext' => new external_value(PARAM_BOOL, 'If enable allow submission using online text', VALUE_OPTIONAL),
            'filetype' => new external_value(PARAM_BOOL, 'If enable allow submission using file', VALUE_OPTIONAL),
            'graded' => new external_value(PARAM_BOOL, 'whether the quiz is graded or not'),
            'maxgrade' => new external_value(PARAM_TEXT, 'Max grade value if the quiz graded', VALUE_OPTIONAL),
            'announcments' => new external_value(PARAM_BOOL, 'The Quiz description'),
            'topic' => new external_value(PARAM_INT, 'If course format is topics, then topic id or section id', VALUE_OPTIONAL),
            'selecteddate' => new external_value(PARAM_TEXT, 'If course format weeks, which topic to insert the course in', VALUE_OPTIONAL),
            'postonedmodo' => new external_value(PARAM_BOOL, 'If enable post the quiz on edmodo lms'),
            'userid' => new external_value(PARAM_INT, 'Current user id'),
            'savehide' => new external_value(PARAM_BOOL, 'If enabled hides module from students'),
            'courses' => new external_multiple_structure(
                new external_value(PARAM_INT, 'Course ID')
            )
        ];

        return new external_function_parameters(
            [
                'assignments' => new external_multiple_structure(
                    new external_single_structure($quizfields)
                )
            ]
        );
    }

    public static function create_assignment($assignments)
    {
        global $DB;

        $result = "";
        $assignmentsresult = "";
        $assignmentandmoduleid = array();

        $debug = fopen("debug.txt", "w") or die("Unable to open file!");

        $params = self::validate_parameters(self::create_assignment_parameters(), array('assignments' => $assignments));

        foreach ($params['assignments'] as $assignment) {
            $usercourses = Get_Courses_User_Teaching($assignment['userid']);

            foreach (array('name', 'description') as $fieldname) {
                if (trim($assignment[$fieldname]) === '') {
                    throw new invalid_parameter_exception('The field ' . $fieldname . ' cannot be blank');
                }
            }

            $grade = 0;
            $filetype = 0;
            $texttype = 0;

            $coursesarray = array();

            $name = $assignment['name'];
            $desc = $assignment['description'];
            $subdate = $assignment['submissionfrom'];
            $duedate = $assignment['due'];
            $showdesc = $assignment['showdescription'];

            $topic = $assignment['topic'];
            $sectionel = $assignment['section'];
            $selectedDate = $assignment['selecteddate'];
            $announcments = $assignment['announcments'];

            $selectedDate = new DateTime();
            $selectedDate->setTimestamp($assignment['selecteddate']);

            $weekSelected = null;
            $topicSelected = null;
            $sectionSelected = null;

            $groupId = null;

            if ($assignment['onlinetext'] == 1) {
                $filetype = $assignment['onlinetext'];
            }

            if ($assignment['filetype'] == 1) {
                $texttype = $assignment['filetype'];
            }

            if ($assignment['graded'] == 1) {
                $grade = $assignment['maxgrade'];
            }

            if ($assignment['savehide'] == 1) {
                $sql = 'UPDATE mdl_school_info SET value = "1" WHERE keyy = "showhide"';
                $DB->execute($sql);
            } else {
                $sql = 'UPDATE mdl_school_info SET value = "0" WHERE keyy = "showhide"';
                $DB->execute($sql);
            }

            foreach ($usercourses as $course) {

                if (in_array($course->cid, $assignment['courses'])) {
                    $crsid = $course->cid;
                    array_push($coursesarray, $crsid);

                    if ($announcments && $announcments > 0) {
                        $weekSelected = null;
                        $topicSelected = null;
                        $sectionSelected = null;
                    } else if ($course->format == 'topics' && $topic > 0) {
                        $topics = array();
                        $topics[0] = "Select Topic";
                        $i = 1;
                        $courseSections = $DB->get_records('course_sections', array('course' => $course->cid));

                        foreach ($courseSections as $section) {
                            if ($section->section > 0) {
                                $topics[$i] = $section->name != null ? $section->name : ('Topic ' . $section->section);
                                if ($i == $topic) {
                                    $topicSelected = $section->section;
                                    break;
                                }
                                $i++;
                            }
                        }
                    } else if ($course->format == 'flexsections' && $sectionel > 0) {
                        $sections = array();
                        $sections[0] = "Select Section";
                        $i = 1;
                        $courseSections = $DB->get_records('course_sections', array('course' => $course->cid));

                        foreach ($courseSections as $section) {
                            if ($section->section > 0) {
                                $sections[$i] = $section->name != null ? $section->name : ('Section ' . $section->section);
                                if ($i == $sectionel) {
                                    $sectionSelected = $section->section;
                                    break;
                                }
                                $i++;
                            }
                        }
                    } else if ($course->format == 'weeks') {
                        $startDate = new DateTime();
                        $startDate->setTimestamp($course->startdate);

                        $endDate = new DateTime();
                        $endDate->setTimestamp($course->enddate);

                        $interval = new DateInterval('P1D');
                        $dateRange = new DatePeriod($startDate, $interval, $endDate);
                        $weekNumber = 1;
                        $start = "";
                        $end = "";
                        $weeks = array();
                        $countDays = 1;

                        foreach ($dateRange as $date) {
                            $weeks[$weekNumber][] = $date;
                            $length = sizeof($weeks['' . $weekNumber . '']);

                            if ($length == 1) {
                                $start = $date;
                            }

                            if ($countDays == 7) {
                                $end = $date;

                                if ($selectedDate >= $start && $selectedDate <= $end) {
                                    $weekSelected = $weekNumber;
                                    break;
                                }

                                $weekNumber++;
                                $countDays = 1;
                            } else {
                                $countDays++;
                            }
                        }
                    } else {
                        continue;
                    }

                    if ($topicSelected && $course->format !== 'topics') {
                        continue;
                    }

                    if ($weekSelected && $course->format !== 'weeks') {
                        continue;
                    }

                    if ($sectionSelected && $course->format !== 'flexsections') {
                        continue;
                    }

                    // $selectedStudents = $_POST["course-" . $crsid . "-students"];

                    // if ($selectedStudents) {
                    //     $groupId = createAndAssignGroupToCourse($crsid, $selectedStudents);
                    // } else {
                    //     $groupId = null;
                    // }

                    $result = Save_Assignment($crsid, $name, $desc, $showdesc, $grade, $subdate, $duedate, $topicSelected, $sectionSelected, $weekSelected, $groupId, $texttype, $filetype);
                    $assignmentandmoduleid = explode(',', $result);
                    $assignmentsresult .= $assignmentandmoduleid[0] . ",";
                }
            }
        }

        fclose($debug);

        return json_encode($assignmentsresult);
    }

    /**
     * Returns description of create_assignment result value
     * @return external_description
     */
    public static function create_assignment_returns()
    {
        return new external_value(PARAM_TEXT, 'Assignments ids seperated by comma');
    }

    /**
     * Returns description of create_folder parameters
     * @return external_function_parameters
     */
    public static function create_folder_parameters()
    {
        $folderfields = [
            'name' => new external_value(PARAM_TEXT, 'The File name'),
            'description' => new external_value(PARAM_TEXT, 'The File description'),
            'announcments' => new external_value(PARAM_BOOL, 'If enabled post the file in announcments section'),
            'topic' => new external_value(PARAM_INT, 'If course format is topics, then topic id or section id', VALUE_OPTIONAL),
            'selecteddate' => new external_value(PARAM_TEXT, 'If course format weeks, which topic to insert the course in', VALUE_OPTIONAL),
            'postonedmodo' => new external_value(PARAM_BOOL, 'If enable post the quiz on edmodo lms'),
            'userid' => new external_value(PARAM_INT, 'Current user id'),
            'savehide' => new external_value(PARAM_BOOL, 'If enabled hides module from students'),
            'courses' => new external_multiple_structure(
                new external_value(PARAM_INT, 'Course ID')
            ),
            'students' => new external_multiple_structure(
                new external_single_structure(
                    array(
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'coursestudents' => new external_multiple_structure(
                            new external_value(PARAM_INT, 'Student Id')
                        ),
                    ),
                    'Array of course students',
                    VALUE_OPTIONAL
                )
            )
        ];

        return new external_function_parameters(
            [
                'folders' => new external_multiple_structure(
                    new external_single_structure($folderfields)
                )
            ]
        );
    }

    public static function create_folder($folders)
    {
        global $DB;

        $params = self::validate_parameters(self::create_folder_parameters(), array('folders' => $folders));

        foreach ($params['folders'] as $folder) {
            $usercourses = Get_Courses_User_Teaching($folder['userid']);

            foreach (array('name') as $fieldname) {
                if (trim($folder[$fieldname]) === '') {
                    throw new invalid_parameter_exception('The field ' . $fieldname . ' cannot be blank');
                }
            }

            $courses = $folder['courses'];
            $students = $folder['students'];

            $name = $folder['name'];
            $desc = $folder['description'];

            $topic = $folder['topic'];
            $sectionel = $folder['section'];
            $selectedDate = $folder['selecteddate'];
            $announcments = $folder['announcments'];

            $selectedDate = new DateTime();
            $selectedDate->setTimestamp($folder['selecteddate']);

            $weekSelected = null;
            $topicSelected = null;
            $sectionSelected = null;

            $groupId = null;


            ($folder['savehide'] == 1)
                ? $DB->execute('UPDATE mdl_school_info SET value = "1" WHERE keyy = "showhide"')
                : $DB->execute('UPDATE mdl_school_info SET value = "0" WHERE keyy = "showhide"');

            foreach ($usercourses as $course) {

                if (in_array($course->cid, $courses)) {

                    if ($announcments && $announcments > 0) {
                        $weekSelected = null;
                        $topicSelected = null;
                        $sectionSelected = null;
                    } else if ($course->format == 'topics' && $topic > 0) {
                        $topics = array();
                        $topics[0] = "Select Topic";
                        $i = 1;
                        $courseSections = $DB->get_records('course_sections', array('course' => $course->cid));

                        foreach ($courseSections as $section) {
                            if ($section->section > 0) {
                                $topics[$i] = $section->name != null ? $section->name : ('Topic ' . $section->section);
                                if ($i == $topic) {
                                    $topicSelected = $section->section;
                                    break;
                                }
                                $i++;
                            }
                        }
                    } else if ($course->format == 'flexsections' && $sectionel > 0) {
                        $sections = array();
                        $sections[0] = "Select Section";
                        $i = 1;
                        $courseSections = $DB->get_records('course_sections', array('course' => $course->cid));

                        foreach ($courseSections as $section) {
                            if ($section->section > 0) {
                                $sections[$i] = $section->name != null ? $section->name : ('Section ' . $section->section);
                                if ($i == $sectionel) {
                                    $sectionSelected = $section->section;
                                    break;
                                }
                                $i++;
                            }
                        }
                    } else if ($course->format == 'weeks') {
                        $startDate = new DateTime();
                        $startDate->setTimestamp($course->startdate);

                        $endDate = new DateTime();
                        $endDate->setTimestamp($course->enddate);

                        $interval = new DateInterval('P1D');
                        $dateRange = new DatePeriod($startDate, $interval, $endDate);
                        $weekNumber = 1;
                        $start = "";
                        $end = "";
                        $weeks = array();
                        $countDays = 1;

                        foreach ($dateRange as $date) {
                            $weeks[$weekNumber][] = $date;
                            $length = sizeof($weeks['' . $weekNumber . '']);

                            if ($length == 1) {
                                $start = $date;
                            }

                            if ($countDays == 7) {
                                $end = $date;

                                if ($selectedDate >= $start && $selectedDate <= $end) {
                                    $weekSelected = $weekNumber;
                                    break;
                                }

                                $weekNumber++;
                                $countDays = 1;
                            } else {
                                $countDays++;
                            }
                        }
                    } else {
                        continue;
                    }

                    if ($topicSelected && $course->format !== 'topics') {
                        continue;
                    }

                    if ($weekSelected && $course->format !== 'weeks') {
                        continue;
                    }

                    if ($sectionSelected && $course->format !== 'flexsections') {
                        continue;
                    }

                    $coursestudents = array_filter($students, function ($student) use ($course) {
                        return ($student['courseid'] == $course->cid) ? true : false;
                    });

                    if ($coursestudents) {
                        foreach ($coursestudents as $studentsids) {
                            $groupId = createAndAssignGroupToCourse($course->cid, $studentsids['coursestudents']);
                        }
                    }

                    $moduleid = Save_Folder($course->cid, $name, $desc, $topicSelected, $sectionSelected, $weekSelected, $groupId);
                    $context = context_module::instance($moduleid);

                    $foldersresult[] = array(
                        'id' => $moduleid,
                        'contextid' => $context->id,
                        'component' => 'mod_folder',
                        'filearea' => 'content',
                        'itemid' => 0,
                        'filepath' => '/',
                        'filename' => $name,
                        'filecontent' => $desc,
                        'contextlevel' => 'course',
                    );
                }
            }
        }

        return $foldersresult;
    }

    /**
     * Returns description of create_folder result value
     * @return external_description
     */
    public static function create_folder_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id'       => new external_value(PARAM_INT, 'student id'),
                    'contextid' => new external_value(PARAM_INT, 'student id'),
                    'component' => new external_value(PARAM_TEXT, 'student firstname'),
                    'filearea' => new external_value(PARAM_TEXT, 'student firstname'),
                    'itemid' => new external_value(PARAM_INT, 'student id'),
                    'filepath' => new external_value(PARAM_TEXT, 'student firstname'),
                    'filename' => new external_value(PARAM_TEXT, 'student firstname'),
                    'filecontent' => new external_value(PARAM_TEXT, 'student firstname'),
                    'contextlevel' => new external_value(PARAM_TEXT, 'student firstname'),
                )
            )
        );
    }

    /**
     * Returns description of create_file parameters
     * @return external_function_parameters
     */
    public static function create_file_parameters()
    {
        $filefields = [
            'name' => new external_value(PARAM_TEXT, 'The File name'),
            'description' => new external_value(PARAM_TEXT, 'The File description'),
            'announcments' => new external_value(PARAM_BOOL, 'If enabled post the file in announcments section'),
            'topic' => new external_value(PARAM_INT, 'If course format is topics, then topic id or section id', VALUE_OPTIONAL),
            'selecteddate' => new external_value(PARAM_TEXT, 'If course format weeks, which topic to insert the course in', VALUE_OPTIONAL),
            'postonedmodo' => new external_value(PARAM_BOOL, 'If enable post the quiz on edmodo lms'),
            'userid' => new external_value(PARAM_INT, 'Current user id'),
            'savehide' => new external_value(PARAM_BOOL, 'If enabled hides module from students'),
            'courses' => new external_multiple_structure(
                new external_value(PARAM_INT, 'Course ID')
            ),
            'students' => new external_multiple_structure(
                new external_single_structure(
                    array(
                        'courseid' => new external_value(PARAM_INT, 'course id'),
                        'coursestudents' => new external_multiple_structure(
                            new external_value(PARAM_INT, 'Student Id')
                        ),
                    ),
                    'Array of course students',
                    VALUE_OPTIONAL
                )
            )
        ];

        return new external_function_parameters(
            [
                'files' => new external_multiple_structure(
                    new external_single_structure($filefields)
                )
            ]
        );
    }

    public static function create_file($files)
    {
        global $DB;

        $params = self::validate_parameters(self::create_file_parameters(), array('files' => $files));

        foreach ($params['files'] as $file) {
            $usercourses = Get_Courses_User_Teaching($file['userid']);

            foreach (array('name') as $fieldname) {
                if (trim($file[$fieldname]) === '') {
                    throw new invalid_parameter_exception('The field ' . $fieldname . ' cannot be blank');
                }
            }

            $courses = $file['courses'];
            $students = $file['students'];

            $name = $file['name'];
            $desc = $file['description'];

            $topic = $file['topic'];
            $sectionel = $file['section'];
            $selectedDate = $file['selecteddate'];
            $announcments = $file['announcments'];

            $selectedDate = new DateTime();
            $selectedDate->setTimestamp($file['selecteddate']);

            $weekSelected = null;
            $topicSelected = null;
            $sectionSelected = null;

            $groupId = null;

            ($file['savehide'] == 1)
                ? $DB->execute('UPDATE mdl_school_info SET value = "1" WHERE keyy = "showhide"')
                : $DB->execute('UPDATE mdl_school_info SET value = "0" WHERE keyy = "showhide"');

            foreach ($usercourses as $course) {

                if (in_array($course->cid, $courses)) {

                    if ($announcments && $announcments > 0) {
                        $weekSelected = null;
                        $topicSelected = null;
                        $sectionSelected = null;
                    } else if ($course->format == 'topics' && $topic > 0) {
                        $topics = array();
                        $topics[0] = "Select Topic";
                        $i = 1;
                        $courseSections = $DB->get_records('course_sections', array('course' => $course->cid));

                        foreach ($courseSections as $section) {
                            if ($section->section > 0) {
                                $topics[$i] = $section->name != null ? $section->name : ('Topic ' . $section->section);
                                if ($i == $topic) {
                                    $topicSelected = $section->section;
                                    break;
                                }
                                $i++;
                            }
                        }
                    } else if ($course->format == 'flexsections' && $sectionel > 0) {
                        $sections = array();
                        $sections[0] = "Select Section";
                        $i = 1;
                        $courseSections = $DB->get_records('course_sections', array('course' => $course->cid));

                        foreach ($courseSections as $section) {
                            if ($section->section > 0) {
                                $sections[$i] = $section->name != null ? $section->name : ('Section ' . $section->section);
                                if ($i == $sectionel) {
                                    $sectionSelected = $section->section;
                                    break;
                                }
                                $i++;
                            }
                        }
                    } else if ($course->format == 'weeks') {
                        $startDate = new DateTime();
                        $startDate->setTimestamp($course->startdate);

                        $endDate = new DateTime();
                        $endDate->setTimestamp($course->enddate);

                        $interval = new DateInterval('P1D');
                        $dateRange = new DatePeriod($startDate, $interval, $endDate);
                        $weekNumber = 1;
                        $start = "";
                        $end = "";
                        $weeks = array();
                        $countDays = 1;

                        foreach ($dateRange as $date) {
                            $weeks[$weekNumber][] = $date;
                            $length = sizeof($weeks['' . $weekNumber . '']);

                            if ($length == 1) {
                                $start = $date;
                            }

                            if ($countDays == 7) {
                                $end = $date;

                                if ($selectedDate >= $start && $selectedDate <= $end) {
                                    $weekSelected = $weekNumber;
                                    break;
                                }

                                $weekNumber++;
                                $countDays = 1;
                            } else {
                                $countDays++;
                            }
                        }
                    } else {
                        continue;
                    }

                    if ($topicSelected && $course->format !== 'topics') {
                        continue;
                    }

                    if ($weekSelected && $course->format !== 'weeks') {
                        continue;
                    }

                    if ($sectionSelected && $course->format !== 'flexsections') {
                        continue;
                    }

                    $coursestudents = array_filter($students, function ($student) use ($course) {
                        return ($student['courseid'] == $course->cid) ? true : false;
                    });

                    if ($coursestudents) {
                        foreach ($coursestudents as $studentsids) {
                            $groupId = createAndAssignGroupToCourse($course->cid, $studentsids['coursestudents']);
                        }
                    }

                    $moduleid = Save_Resource($course->cid, $name, $desc, $topicSelected, $sectionSelected, $weekSelected, $groupId);
                    $context = context_module::instance($moduleid);

                    $filesresult[] = array(
                        'id' => $moduleid,
                        'contextid' => $context->id,
                        'component' => 'mod_resource',
                        'filearea' => 'content',
                        'itemid' => 0,
                        'filepath' => '/',
                        'filename' => $name,
                        'filecontent' => $desc,
                        'contextlevel' => 'course',
                    );
                }
            }
        }

        return $filesresult;
    }

    /**
     * Returns description of create_file result value
     * @return external_description
     */
    public static function create_file_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id'       => new external_value(PARAM_INT, 'student id'),
                    'contextid' => new external_value(PARAM_INT, 'student id'),
                    'component' => new external_value(PARAM_TEXT, 'student firstname'),
                    'filearea' => new external_value(PARAM_TEXT, 'student firstname'),
                    'itemid' => new external_value(PARAM_INT, 'student id'),
                    'filepath' => new external_value(PARAM_TEXT, 'student firstname'),
                    'filename' => new external_value(PARAM_TEXT, 'student firstname'),
                    'filecontent' => new external_value(PARAM_TEXT, 'student firstname'),
                    'contextlevel' => new external_value(PARAM_TEXT, 'student firstname'),
                )
            )
        );
    }

    /**
     * Returns description of create_label parameters
     * @return external_function_parameters
     */
    public static function create_label_parameters()
    {
        $labelfields = [
            'text' => new external_value(PARAM_TEXT, 'The Label text'),
            'announcments' => new external_value(PARAM_BOOL, 'The Quiz description'),
            'topic' => new external_value(PARAM_INT, 'If course format is topics, then topic id or section id', VALUE_OPTIONAL),
            'selecteddate' => new external_value(PARAM_TEXT, 'If course format weeks, which topic to insert the course in', VALUE_OPTIONAL),
            'postonedmodo' => new external_value(PARAM_BOOL, 'If enable post the quiz on edmodo lms'),
            'userid' => new external_value(PARAM_INT, 'Current user id'),
            'savehide' => new external_value(PARAM_BOOL, 'If enabled hides module from students'),
            'courses' => new external_multiple_structure(
                new external_value(PARAM_INT, 'Course ID')
            )
        ];

        return new external_function_parameters(
            [
                'labels' => new external_multiple_structure(
                    new external_single_structure($labelfields)
                )
            ]
        );
    }

    public static function create_label($labels)
    {
        global $DB;

        $result = "";
        $labelsresult = "";
        $labelandmoduleid = array();

        $debug = fopen("debug.txt", "w") or die("Unable to open file!");

        $params = self::validate_parameters(self::create_label_parameters(), array('labels' => $labels));

        foreach ($params['labels'] as $label) {
            $usercourses = Get_Courses_User_Teaching($label['userid']);

            foreach (array('text') as $fieldname) {
                if (trim($label[$fieldname]) === '') {
                    throw new invalid_parameter_exception('The field ' . $fieldname . ' cannot be blank');
                }
            }

            $coursesarray = array();

            $desc = $label['text'];

            $topic = $label['topic'];
            $sectionel = $label['section'];
            $selectedDate = $label['selecteddate'];
            $announcments = $label['announcments'];

            $selectedDate = new DateTime();
            $selectedDate->setTimestamp($label['selecteddate']);

            $weekSelected = null;
            $topicSelected = null;
            $sectionSelected = null;

            $groupId = null;

            if ($label['savehide'] == 1) {
                $sql = 'UPDATE mdl_school_info SET value = "1" WHERE keyy = "showhide"';
                $DB->execute($sql);
            } else {
                $sql = 'UPDATE mdl_school_info SET value = "0" WHERE keyy = "showhide"';
                $DB->execute($sql);
            }

            foreach ($usercourses as $course) {

                if (in_array($course->cid, $label['courses'])) {
                    $crsid = $course->cid;
                    array_push($coursesarray, $crsid);

                    if ($announcments && $announcments > 0) {
                        $weekSelected = null;
                        $topicSelected = null;
                        $sectionSelected = null;
                    } else if ($course->format == 'topics' && $topic > 0) {
                        $topics = array();
                        $topics[0] = "Select Topic";
                        $i = 1;
                        $courseSections = $DB->get_records('course_sections', array('course' => $course->cid));

                        foreach ($courseSections as $section) {
                            if ($section->section > 0) {
                                $topics[$i] = $section->name != null ? $section->name : ('Topic ' . $section->section);
                                if ($i == $topic) {
                                    $topicSelected = $section->section;
                                    break;
                                }
                                $i++;
                            }
                        }
                    } else if ($course->format == 'flexsections' && $sectionel > 0) {
                        $sections = array();
                        $sections[0] = "Select Section";
                        $i = 1;
                        $courseSections = $DB->get_records('course_sections', array('course' => $course->cid));

                        foreach ($courseSections as $section) {
                            if ($section->section > 0) {
                                $sections[$i] = $section->name != null ? $section->name : ('Section ' . $section->section);
                                if ($i == $sectionel) {
                                    $sectionSelected = $section->section;
                                    break;
                                }
                                $i++;
                            }
                        }
                    } else if ($course->format == 'weeks') {
                        $startDate = new DateTime();
                        $startDate->setTimestamp($course->startdate);

                        $endDate = new DateTime();
                        $endDate->setTimestamp($course->enddate);

                        $interval = new DateInterval('P1D');
                        $dateRange = new DatePeriod($startDate, $interval, $endDate);
                        $weekNumber = 1;
                        $start = "";
                        $end = "";
                        $weeks = array();
                        $countDays = 1;

                        foreach ($dateRange as $date) {
                            $weeks[$weekNumber][] = $date;
                            $length = sizeof($weeks['' . $weekNumber . '']);

                            if ($length == 1) {
                                $start = $date;
                            }

                            if ($countDays == 7) {
                                $end = $date;

                                if ($selectedDate >= $start && $selectedDate <= $end) {
                                    $weekSelected = $weekNumber;
                                    break;
                                }

                                $weekNumber++;
                                $countDays = 1;
                            } else {
                                $countDays++;
                            }
                        }
                    } else {
                        continue;
                    }

                    if ($topicSelected && $course->format !== 'topics') {
                        continue;
                    }

                    if ($weekSelected && $course->format !== 'weeks') {
                        continue;
                    }

                    if ($sectionSelected && $course->format !== 'flexsections') {
                        continue;
                    }

                    // $selectedStudents = $_POST["course-" . $crsid . "-students"];

                    // if ($selectedStudents) {
                    //     $groupId = createAndAssignGroupToCourse($crsid, $selectedStudents);
                    // } else {
                    //     $groupId = null;
                    // }

                    $result = Save_Label($crsid, $desc, $topicSelected, $sectionSelected, $weekSelected, $groupId);

                    $labelandmoduleid = explode(',', $result);
                    $labelsresult .= $labelandmoduleid[0];
                }
            }
        }

        fclose($debug);

        return json_encode($labelsresult);
    }

    /**
     * Returns description of create_label result value
     * @return external_description
     */
    public static function create_label_returns()
    {
        return new external_value(PARAM_TEXT, 'Labels ids seperated by comma');
    }

    /**
     * Returns description of create_url parameters
     * @return external_function_parameters
     */
    public static function create_url_parameters()
    {
        $urlfields = [
            'name' => new external_value(PARAM_TEXT, 'The Quiz name'),
            'url' => new external_value(PARAM_TEXT, 'The Quiz name'),
            'description' => new external_value(PARAM_TEXT, 'The Quiz description', VALUE_OPTIONAL),
            'announcments' => new external_value(PARAM_BOOL, 'The Quiz description'),
            'topic' => new external_value(PARAM_INT, 'If course format is topics, then topic id or section id', VALUE_OPTIONAL),
            'selecteddate' => new external_value(PARAM_TEXT, 'If course format weeks, which topic to insert the course in', VALUE_OPTIONAL),
            'postonedmodo' => new external_value(PARAM_BOOL, 'If enable post the quiz on edmodo lms'),
            'userid' => new external_value(PARAM_INT, 'Current user id'),
            'savehide' => new external_value(PARAM_BOOL, 'If enabled hides module from students'),
            'courses' => new external_multiple_structure(
                new external_value(PARAM_INT, 'Course ID')
            )
        ];

        return new external_function_parameters(
            [
                'urls' => new external_multiple_structure(
                    new external_single_structure($urlfields)
                )
            ]
        );
    }

    public static function create_url($urls)
    {
        global $DB, $CFG;

        $params = self::validate_parameters(self::create_url_parameters(), array('urls' => $urls));

        $result = "";
        $urlsresult = "";

        $debug = fopen("debug.txt", "w") or die("Unable to open file!");

        foreach ($params['urls'] as $url) {
            $usercourses = Get_Courses_User_Teaching($url['userid']);

            foreach (array('name', 'url') as $fieldname) {
                if (trim($url[$fieldname]) === '') {
                    throw new invalid_parameter_exception('The field ' . $fieldname . ' cannot be blank');
                }
            }

            $coursesarray = array();
            $courses = $url['courses'];

            $name = $url['name'];
            $url = $url['url'];
            $desc = $url['description'];

            $topic = $url['topic'];
            $sectionel = $url['section'];
            $selectedDate = $url['selecteddate'];
            $announcments = $url['announcments'];

            $selectedDate = new DateTime();
            $selectedDate->setTimestamp($url['selecteddate']);

            $weekSelected = null;
            $topicSelected = null;
            $sectionSelected = null;

            $groupId = null;


            if ($url['savehide'] == 1) {
                $sql = 'UPDATE mdl_school_info SET value = "1" WHERE keyy = "showhide"';
                $DB->execute($sql);
            } else {
                $sql = 'UPDATE mdl_school_info SET value = "0" WHERE keyy = "showhide"';
                $DB->execute($sql);
            }

            foreach ($usercourses as $course) {

                if (in_array($course->cid, $courses)) {
                    $crsid = $course->cid;
                    array_push($coursesarray, $crsid);

                    if ($announcments && $announcments > 0) {
                        $weekSelected = null;
                        $topicSelected = null;
                        $sectionSelected = null;
                    } else if ($course->format == 'topics' && $topic > 0) {
                        $topics = array();
                        $topics[0] = "Select Topic";
                        $i = 1;
                        $courseSections = $DB->get_records('course_sections', array('course' => $course->cid));

                        foreach ($courseSections as $section) {
                            if ($section->section > 0) {
                                $topics[$i] = $section->name != null ? $section->name : ('Topic ' . $section->section);
                                if ($i == $topic) {
                                    $topicSelected = $section->section;
                                    break;
                                }
                                $i++;
                            }
                        }
                    } else if ($course->format == 'flexsections' && $sectionel > 0) {
                        $sections = array();
                        $sections[0] = "Select Section";
                        $i = 1;
                        $courseSections = $DB->get_records('course_sections', array('course' => $course->cid));

                        foreach ($courseSections as $section) {
                            if ($section->section > 0) {
                                $sections[$i] = $section->name != null ? $section->name : ('Section ' . $section->section);
                                if ($i == $sectionel) {
                                    $sectionSelected = $section->section;
                                    break;
                                }
                                $i++;
                            }
                        }
                    } else if ($course->format == 'weeks') {
                        $startDate = new DateTime();
                        $startDate->setTimestamp($course->startdate);

                        $endDate = new DateTime();
                        $endDate->setTimestamp($course->enddate);

                        $interval = new DateInterval('P1D');
                        $dateRange = new DatePeriod($startDate, $interval, $endDate);
                        $weekNumber = 1;
                        $start = "";
                        $end = "";
                        $weeks = array();
                        $countDays = 1;

                        foreach ($dateRange as $date) {
                            $weeks[$weekNumber][] = $date;
                            $length = sizeof($weeks['' . $weekNumber . '']);

                            if ($length == 1) {
                                $start = $date;
                            }

                            if ($countDays == 7) {
                                $end = $date;

                                if ($selectedDate >= $start && $selectedDate <= $end) {
                                    $weekSelected = $weekNumber;
                                    break;
                                }

                                $weekNumber++;
                                $countDays = 1;
                            } else {
                                $countDays++;
                            }
                        }
                    } else {
                        continue;
                    }

                    if ($topicSelected && $course->format !== 'topics') {
                        continue;
                    }

                    if ($weekSelected && $course->format !== 'weeks') {
                        continue;
                    }

                    if ($sectionSelected && $course->format !== 'flexsections') {
                        continue;
                    }

                    // $selectedStudents = $_POST["course-" . $crsid . "-students"];

                    // if ($selectedStudents) {
                    //     $groupId = createAndAssignGroupToCourse($crsid, $selectedStudents);
                    // } else {
                    //     $groupId = null;
                    // }

                    $result = Save_URL($crsid, $name, $url, $desc, $topicSelected, $sectionSelected, $weekSelected, $groupId);

                    $urlsresult = $CFG->wwwroot . '/mod/url/view.php?id=' . $result;
                }
            }
        }

        fclose($debug);

        return json_encode($urlsresult);
    }

    /**
     * Returns description of create_url result value
     * @return external_description
     */
    public static function create_url_returns()
    {
        return new external_value(PARAM_TEXT, 'urls ids seperated by comma');
    }

    /**
     * Returns description of get_students_in_course parameters
     * @return external_function_parameters
     */
    public static function get_students_in_course_parameters()
    {
        return new external_function_parameters(
            array(
                "courseid" => new external_value(PARAM_TEXT, "The Course ID"),
            )
        );
    }

    public static function get_students_in_course($courseid)
    {
        global $DB;

        $params = self::validate_parameters(self::get_students_in_course_parameters(), array('courseid' => $courseid));

        $students = $DB->get_records_sql('SELECT Distinct u.id, u.firstname, u.lastname FROM mdl_course c JOIN mdl_context cx ON c.id = cx.instanceid JOIN mdl_role_assignments ra ON cx.id = ra.contextid AND ra.roleid = \'5\' JOIN mdl_user u ON ra.userid = u.id Where cx.contextlevel = \'50\'and c.id IN (' . $params['courseid'] . ')');

        foreach ($students as $student) {
            $coursestudents[] = array('id' => $student->id, 'firstname' => $student->firstname,  'lastname' => $student->lastname, 'courseid' => $params['courseid']);
        }

        return $coursestudents;
    }

    /**
     * Returns description of get_students_in_course result value
     * @return external_description
     */
    public static function get_students_in_course_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id'       => new external_value(PARAM_INT, 'student id'),
                    'firstname'       => new external_value(PARAM_TEXT, 'student firstname'),
                    'lastname'       => new external_value(PARAM_TEXT, 'student lastname'),
                    'courseid'       => new external_value(PARAM_INT, 'course id'),
                )
            )
        );
    }

    /**
     * Returns description of upload parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function upload_parameters()
    {
        return new external_function_parameters(
            array(
                'contextid' => new external_value(PARAM_INT, 'context id', VALUE_DEFAULT, null),
                'component' => new external_value(PARAM_COMPONENT, 'component'),
                'filearea'  => new external_value(PARAM_AREA, 'file area'),
                'itemid'    => new external_value(PARAM_INT, 'associated id'),
                'filepath'  => new external_value(PARAM_PATH, 'file path'),
                'filename'  => new external_value(PARAM_FILE, 'file name'),
                'filecontent' => new external_value(PARAM_TEXT, 'file content'),
                'contextlevel' => new external_value(PARAM_ALPHA, 'The context level to put the file in,
                        (block, course, coursecat, system, user, module)', VALUE_DEFAULT, null),
                'instanceid' => new external_value(PARAM_INT, 'The Instance id of item associated
                         with the context level', VALUE_DEFAULT, null)
            )
        );
    }

    /**
     * Uploading a file to moodle
     *
     * @param int    $contextid    context id
     * @param string $component    component
     * @param string $filearea     file area
     * @param int    $itemid       item id
     * @param string $filepath     file path
     * @param string $filename     file name
     * @param string $filecontent  file content
     * @param string $contextlevel Context level (block, course, coursecat, system, user or module)
     * @param int    $instanceid   Instance id of the item associated with the context level
     * @return array
     * @since Moodle 2.2
     */
    public static function upload($contextid, $component, $filearea, $itemid, $filepath, $filename, $filecontent, $contextlevel, $instanceid)
    {
        global $CFG;

        require_once("$CFG->libdir/externallib.php");
        require_once("$CFG->libdir/filelib.php");

        $fileinfo = self::validate_parameters(self::upload_parameters(), array(
            'contextid' => $contextid, 'component' => $component, 'filearea' => $filearea, 'itemid' => $itemid,
            'filepath' => $filepath, 'filename' => $filename, 'filecontent' => $filecontent, 'contextlevel' => $contextlevel,
            'instanceid' => $instanceid
        ));

        if (!isset($fileinfo['filecontent'])) {
            throw new moodle_exception('nofile');
        }
        // Saving file.
        $dir = make_temp_directory('wsupload');

        if (empty($fileinfo['filename'])) {
            $filename = uniqid('wsupload', true) . '_' . time() . '.tmp';
        } else {
            $filename = $fileinfo['filename'];
        }

        if (file_exists($dir . $filename)) {
            $savedfilepath = $dir . uniqid('m') . $filename;
        } else {
            $savedfilepath = $dir . $filename;
        }

        file_put_contents($savedfilepath, base64_decode($fileinfo['filecontent']));
        // file_put_contents($savedfilepath, $fileinfo['filecontent']);
        @chmod($savedfilepath, $CFG->filepermissions);

        unset($fileinfo['filecontent']);

        if (!empty($fileinfo['filepath'])) {
            $filepath = $fileinfo['filepath'];
        } else {
            $filepath = '/';
        }

        $itemid = 0;
        if (isset($fileinfo['itemid'])) {
            $itemid = $fileinfo['itemid'];
        }
        if ($filearea == 'draft' && $itemid <= 0) {
            // Generate a draft area for the files.
            $itemid = file_get_unused_draft_itemid();
        } else if ($filearea == 'private') {
            // TODO MDL-31116 in user private area, itemid is always 0.
            $itemid = 0;
        }

        // We need to preserve backword compatibility. Context id is no more a required.
        if (empty($fileinfo['contextid'])) {
            unset($fileinfo['contextid']);
        }

        // Get and validate context.
        $context = self::get_context_from_params($fileinfo);
        self::validate_context($context);
        if (($fileinfo['component'] == 'user' and $fileinfo['filearea'] == 'private')) {
            throw new moodle_exception('privatefilesupload');
        }

        $browser = get_file_browser();

        // Check existing file.
        if ($file = $browser->get_file_info($context, $component, $filearea, $itemid, $filepath, $filename)) {
            throw new moodle_exception('fileexist');
        }

        // Move file to filepool.
        if ($dir = $browser->get_file_info($context, $component, $filearea, $itemid, $filepath, '.')) {
            $info = $dir->create_file_from_pathname($filename, $savedfilepath);
            $params = $info->get_params();
            unlink($savedfilepath);
            return array(
                'contextid' => $params['contextid'],
                'component' => $params['component'],
                'filearea' => $params['filearea'],
                'itemid' => $params['itemid'],
                'filepath' => $params['filepath'],
                'filename' => $params['filename'],
                'url' => $info->get_url()
            );
        } else {
            throw new moodle_exception('nofile');
        }
    }

    /**
     * Returns description of upload returns
     *
     * @return external_single_structure
     * @since Moodle 2.2
     */
    public static function upload_returns()
    {
        return new external_single_structure(
            array(
                'contextid' => new external_value(PARAM_INT, ''),
                'component' => new external_value(PARAM_COMPONENT, ''),
                'filearea'  => new external_value(PARAM_AREA, ''),
                'itemid'   => new external_value(PARAM_INT, ''),
                'filepath' => new external_value(PARAM_TEXT, ''),
                'filename' => new external_value(PARAM_FILE, ''),
                'url'      => new external_value(PARAM_TEXT, ''),
            )
        );
    }
}
