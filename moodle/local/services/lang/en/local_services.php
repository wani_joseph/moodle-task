<?php
// This file is part of the local entrepreware plugin for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language strings
 *
 * @package    local_services
 * @copyright  2013 onwards - University of Nottingham <www.nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Services';
$string['active'] = 'Enabled send course message to Entrepreware';
$string['stractive'] = 'Enables send course message to Entrepreware system.';
$string['quiz'] = "يوجد كويز جديد علي ال (LMS) برجاء الدخول علي الرابط للمتابعة";
$string['assignment'] = "يوجد مهام جديد علي ال (LMS) برجاء الدخول علي الرابط للمتابعة";
$string['label'] = "يوجد عنوان جديد علي ال (LMS) برجاء الدخول علي الرابط للمتابعة";
$string['url'] = "يوجد رابط جديد علي ال (LMS) برجاء الدخول علي الرابط للمتابعة";
$string['resource'] = "يوجد ملف جديد علي ال (LMS) برجاء الدخول علي الرابط للمتابعة";
$string['messageprovider:notificationent_newresource'] = 'New Resource has been created';
$string['confirm'] = 'Vist course page in order to confirm the new resource';
