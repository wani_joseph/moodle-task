<?php
// This file is part of the local entrepreware plugin for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_services
 * @copyright  2014 onwards - University of Nottingham <www.nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

if (!isset($_COOKIE['username'])) {

    $userName = get_e4e_user();
    $userPass = get_e4e_password();

    setcookie('username', $userName, time() + (86400 * 6), "/");
    setcookie('password', $userPass, time() + (86400 * 6), "/");

    $_COOKIE['username'] = $userName; //"mohammad@entrepreware.com";//mohamedali@entrepreware.com
    $_COOKIE['password'] = $userPass; //"123456789";//987654321
}

/**
 * Check if current user is a teacher
 *
 * @param $id
 * @return boolean
 */
function is_teacher($id)
{
    global $DB;

    $roles = $DB->get_records_sql('SELECT mdl_role.shortname FROM mdl_role,mdl_role_assignments,mdl_user WHERE mdl_role_assignments.userid= mdl_user.id and mdl_role_assignments.roleid = mdl_role.id and mdl_user.id = ? ', array($id));
    $sroles = "";

    foreach ($roles as $key) {
        $sroles .= $key->shortname . " ";
    }

    if (preg_match('/editingteacher/', $sroles) || preg_match('/teacher/', $sroles)) {
        return TRUE;
    }
}

/**
 * Check if current user has approve capability
 *
 * @param $id
 * @return boolean
 */
function can_approve($id)
{

    global $DB;

    $roles = $DB->get_records_sql('SELECT mdl_role.shortname FROM mdl_role,mdl_role_assignments,mdl_user WHERE mdl_role_assignments.userid= mdl_user.id and mdl_role_assignments.roleid = mdl_role.id and mdl_user.id = ? ', array($id));

    foreach ($roles as $key) {

        if ($key->shortname == 'head' || $key->shortname == 'supervisor' || $key->shortname == 'reporting' || $key->shortname == 'manager') {
            return true;
        }
    }

    return false;
}

/**
 * Check if current user has show course module capability
 *
 * @param $id
 * @return boolean
 */
function can_show($id)
{

    global $DB;

    $roles = $DB->get_records_sql('SELECT mdl_role.shortname FROM mdl_role,mdl_role_assignments,mdl_user WHERE mdl_role_assignments.userid= mdl_user.id and mdl_role_assignments.roleid = mdl_role.id and mdl_user.id = ? ', array($id));

    foreach ($roles as $key) {
        if ($key->shortname == 'teacher' || $key->shortname == 'editingteacher') {
            return true;
        }
    }

    return false;
}

/**
 * Check if current user is a student
 *
 * @param $id
 * @return boolean
 */
function is_student($id)
{
    global $DB;

    $roles = $DB->get_records_sql('SELECT mdl_role.shortname FROM mdl_role,mdl_role_assignments,mdl_user WHERE mdl_role_assignments.userid= mdl_user.id and mdl_role_assignments.roleid = mdl_role.id and mdl_user.id = ? ', array($id));
    $sroles = "";

    foreach ($roles as $key) {
        $sroles .= $key->shortname . " ";
    }

    if (preg_match('/student/', $sroles)) {
        return TRUE;
    }
}

/**
 * Check if current user is a parent
 *
 * @param $id
 * @return boolean
 */
function is_parent($id)
{
    global $DB;

    $roles = $DB->get_records_sql('SELECT mdl_role.shortname FROM mdl_role,mdl_role_assignments,mdl_user WHERE mdl_role_assignments.userid= mdl_user.id and mdl_role_assignments.roleid = mdl_role.id and mdl_user.id = ? ', array($id));
    $sroles = "";

    foreach ($roles as $key) {
        $sroles .= $key->shortname . " ";
    }

    if (preg_match('/parent/', $sroles)) {
        return TRUE;
    }
}

/**
 * Check if current user is a supervisor
 *
 * @param $id
 * @return boolean
 */
function is_supervisor($id)
{

    global $DB;

    $roles = $DB->get_records_sql('SELECT mdl_role.shortname FROM mdl_role,mdl_role_assignments,mdl_user WHERE mdl_role_assignments.userid= mdl_user.id and mdl_role_assignments.roleid = mdl_role.id and mdl_user.id = ? ', array($id));
    $sroles = "";

    foreach ($roles as $key) {
        $sroles .= $key->shortname . " ";
    }

    if (preg_match('/head/', $sroles) || preg_match('/supervisor/', $sroles) ||  preg_match('/manager/', $sroles)) {

        return TRUE;
    }
}

/**
 * Fetch e4e username from school info table
 *
 * @param $id
 * @return string
 */
function get_e4e_user()
{
    global $DB;

    // $user = $DB->get_record_sql('select * from mdl_school_info where keyy="username"  LIMIT 1');

    // return ($user) ? $user->value : "";

    return "";
}

/**
 * Fetch e4e password from school info table
 *
 * @param $id
 * @return string
 */
function get_e4e_password()
{
    global $DB;

    // $password = $DB->get_record_sql('select * from mdl_school_info where keyy="password"  LIMIT 1');

    // return ($password) ? $password->value : "";

    return "";
}

/**
 * Fetch students access code in a givern course
 *
 * @param $courseid
 * @return array
 */
function get_students_access_codes($courseid)
{
    global $DB;
    $accesscodes = array();

    $students = $DB->get_records_sql('SELECT u.* FROM mdl_user u INNER JOIN mdl_role_assignments ra ON ra.userid = u.id INNER JOIN mdl_context ct ON ct.id = ra.contextid INNER JOIN mdl_course c ON c.id = ct.instanceid INNER JOIN mdl_role r ON r.id = ra.roleid WHERE r.id = 5 and c.id = ?', array($courseid));

    if ($students) {
        $i = 0;

        foreach ($students as $student) {
            $accesscode = $DB->get_record_sql('select ud.data from mdl_user_info_field as uf , mdl_user_info_data as ud where uf.shortname="AccessCode" and ud.fieldid=uf.id and ud.userid=? limit 1', array($student->id));

            if ($accesscode) {
                $accesscodes[$i] = $accesscode->data;
                $i++;
            }
        }
    }

    return $accesscodes;
}

/**
 * Send notification message to heads
 *
 * @param $headsids
 * an arrays include ids of heads users in a given course
 * @param $courseurl
 * course url so the head can approve the module follow the course
 * @return void
 */
function send_messages_to_heads($headsids, $courseurl)
{
    global $DB, $USER;

    foreach ($headsids as $headsid) {
        $user = $DB->get_record('user', array('id' => $headsid));

        $message = new \core\message\message();
        $message->component = 'block_quick_actions'; // Your plugin's name
        $message->name = 'notificationent_newresource'; // Your notification name from message.php
        $message->userfrom = $USER; // If the message is 'from' a specific user you can set them here
        $message->userto = $user;
        $message->subject = get_string('messageprovider:notificationent_newresource', 'local_services');
        $message->fullmessage = '';
        $message->fullmessageformat = FORMAT_HTML;
        $message->fullmessagehtml = '<p><a href="' . $courseurl . '">Confirm</a></p>';
        $message->smallmessage = '';
        $message->notification = 1; // Because this is a notification generated from Moodle, not a user-to-user message
        $message->contexturl = $courseurl;

        // Actually send the message
        message_send($message);
    }
}

/**
 * Authenticate e4e user given
 *
 * @param $username
 * @param $password
 * @return void
 */
function authenticate_e4e_user($username, $password)
{
    $refreshToken = '';
    $url = 'https://education.entrepreware.com/oauth/token?grant_type=password&client_id=my-trusted-client&password=' . $password . '&username=' . $username;
    $json = file_get_contents($url);

    if ($json) {
        $_COOKIE['username'] = $username;
        $_COOKIE['password'] = $password;
        $json_data = json_decode($json, true);
        $refreshToken = $json_data["refreshToken"]['value'];

        if ($refreshToken) {
            setcookie('refreshtoken', $refreshToken, time() + (86400 * 6), "/");
            authenticate_by_refresh_token($refreshToken);
        }
    } else {
        $_COOKIE['username'] = "";
        $_COOKIE['password'] = "";
    }
}

/**
 * Authenticate e4e user given
 *
 * @param $refreshToken
 * @return void
 */
function authenticate_by_refresh_token($refreshToken)
{
    $accessToken = '';
    $url =  'https://education.entrepreware.com/oauth/token?grant_type=refresh_token&client_id=my-trusted-client&refresh_token=' . $refreshToken;
    $json = file_get_contents($url);

    if ($json) {
        $_SESSION['refresh_token'] = $refreshToken;
        $json_data = json_decode($json, true);
        $accessToken = $json_data['value'];

        if ($accessToken) {
            setcookie('accesstoken', $accessToken, time() + (86400 * 6), "/");
        }
    } else {
        $_COOKIE['refresh_token'] = "";

        if ($_COOKIE['username'] && $_COOKIE['password']) {
            authenticate_e4e_user($_COOKIE['username'], $_COOKIE['username']);
        } else {
            \core\notification::error("Error Refresh Token Not Correct");
        }
    }
}

/**
 * Send notification message to e4e given the params
 *
 * @param $action
 * @param $url
 * @param $img
 * @param $studentAccessCodeList
 * @param $emailsList
 * @return void
 */
function send_moodle_notifications($action, $url, $img, $studentAccessCodeList, $emailsList)
{
    $attachmentUrlList = array($img);

    if (!isset($_COOKIE['accesstoken'])) {

        if (isset($_COOKIE['refreshtoken'])) {
            authenticate_by_refresh_token($_COOKIE['refreshtoken']);
        } else {
            authenticate_e4e_user($_COOKIE['username'], $_COOKIE['password']);
        }
    } else {
        $accesstoken = "Bearer " . $_COOKIE['accesstoken'];
    }

    $data = array(
        'accountId' => '1000', // no matter this property it may be used in future.
        'title' => "Moodle Notification",
        'body' => $action . " " . $url,
        'username' => 'entrepreware-user',
        'attachmentUrlList' => $attachmentUrlList,
        'studentAccessCodeList' => $studentAccessCodeList,
        'studentEmailList' => $emailsList
    );

    $url = 'https://education.entrepreware.com/authentication/moodlemessage.ent';
    $content = json_encode($data);

    $curl = curl_init($url);

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt(
        $curl,
        CURLOPT_HTTPHEADER,
        array("Content-type: application/json", "Authorization: " . $accesstoken)
    );
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

    $json_response = curl_exec($curl);

    curl_getinfo($curl, CURLINFO_HTTP_CODE);

    curl_close($curl);

    json_decode($json_response, true);
}

function call_api($method, $url, $data = false)
{
    if (!isset($_COOKIE['accesstoken'])) {

        if (isset($_COOKIE['refreshtoken'])) {

            authenticate_by_refresh_token($_COOKIE['refreshtoken']);
        } else {
            authenticate_e4e_user($_COOKIE['username'], $_COOKIE['password']);
        }
    } else {
        $accesstoken = "Bearer " . $_COOKIE['accesstoken'];
    }

    $accesstoken = $_COOKIE['accesstoken'];

    if ($accesstoken) {
        $content = $data;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $curl,
            CURLOPT_HTTPHEADER,
            array("Content-type: application/json", "Authorization: " . $accesstoken)
        );
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        $json_response = curl_exec($curl);
        curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $result = $json_response;

        return $result;
    } else {
        \core\notification::error("Error Connecting e4e user:" . $_COOKIE['username'] . " , with pass:" . $_COOKIE['password'] . ". contact Entrepreware support to change credentials in School_info.");
    }
}

function enableGroupModeForCourse($courseId)
{
    global $DB;

    $DB->execute('update mdl_course set groupmode = 1 where id = ' . $courseId);
}

function getStudentsNames($studentsIds)
{
    global $DB;

    $students = $DB->get_records_sql("select CONCAT(u.firstname, ' ', u.lastname) as name from mdl_user u where u.id in ( " . implode(',', $studentsIds)  . ")");

    $mapFunc = function ($value) {
        return $value->name;
    };

    $studentsNames = array_values(array_map($mapFunc, $students));

    return $studentsNames;
}

function createAndAssignGroupToCourse($courseId, $studentsIds)
{
    global $CFG;

    require_once($CFG->dirroot . '/group/lib.php');

    enableGroupModeForCourse($courseId);

    $groupData = new stdClass();
    $groupData->courseid = $courseId;
    $studentsNames = getStudentsNames($studentsIds);
    $groupData->name = implode(", ", $studentsNames);
    $newgroupid = groups_create_group($groupData);

    foreach ($studentsIds as $id) {
        groups_add_member($newgroupid, $id);
    }

    return $newgroupid;
}

function Get_Courses_User_Teaching($userid)
{
    global $DB;
    $courses = $DB->get_records_sql('SELECT c.id as cid,c.fullname as fullname,c.format,c.startdate,c.enddate,cc.name ,cc.id FROM mdl_user u INNER JOIN mdl_role_assignments ra ON ra.userid = u.id INNER JOIN mdl_context ct ON ct.id = ra.contextid INNER JOIN mdl_course c ON c.id = ct.instanceid INNER JOIN mdl_role r ON r.id = ra.roleid INNER JOIN mdl_course_categories cc ON c.category = cc.id WHERE r.id in (3,4,1,16,15) and u.id=? and c.visible=true', array($userid));

    return $courses;
}

function Save_Quiz($crsid, $name, $desc, $grade, $openquiz, $closequiz, $quiztiming, $topic, $section, $week, $groupId)
{
    global $DB;

    $returnstring = "";

    $quizdata = array(
        'course' =>  $crsid,
        'name' => $name,
        'intro' => $desc,
        'introformat' => "1",
        'timeopen' => $openquiz,
        'timeclose' => $closequiz,
        'timelimit' => $quiztiming,
        'overduehandling' => 'autosubmit',
        'preferredbehaviour' => "deferredfeedback",
        'reviewattempt' => "69904",
        'reviewcorrectness' => "4368",
        'reviewmarks' => "4368",
        'reviewspecificfeedback' => "4368",
        'reviewgeneralfeedback' => "4368",
        'reviewrightanswer' => "4368",
        'reviewoverallfeedback' => "4368",
        'questionsperpage' => "1",
        'navmethod' => "free",
        'shuffleanswers' => "1",
        'sumgrades' => "0",
        'grade' => $grade,
        'timecreated' => time(),
        'timemodified' => time()
    );
    $quizdata = (object) $quizdata;

    $quizid = $DB->insert_record('quiz', $quizdata, true, false);

    if ($quizid) {
        $returnstring .= $quizid . ',';

        $quizsecdata = array('quizid' =>  $quizid, 'firstslot' => "1");
        $quizsecdata = (object) $quizsecdata;

        $quizsecid = $DB->insert_record('quiz_sections', $quizsecdata, true, false);

        if ($quizsecid) {
            $quizfeedbackdata = array('quizid' =>  $quizid, 'feedbacktext' => "", 'maxgrade' => $grade);
            $quizfeedbackdata = (object) $quizfeedbackdata;

            $DB->insert_record('quiz_feedback', $quizfeedbackdata, true, false);

            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => 0));

            if ($topic && $topic > 0) {
                $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $topic));
            }

            if ($week && $week > 0) {
                $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $week));
            }

            if ($section && $section > 0) {
                $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $section));
            }

            $cmdata = array(
                'course' => $crsid,
                'module' => '16',
                'instance' => $quizid,
                'section' => $sections->id,
                'added' => time(),
            );

            if ($groupId) {
                $availability = '{\"op\":\"&\",\"c\":[{\"type\":\"group\",\"id\":' . $groupId . '}],\"showc\":[false]}';

                $cmdata = array(
                    'course' => $crsid,
                    'module' => '16',
                    'instance' => $quizid,
                    'section' => $sections->id,
                    'added' => time(),
                    'groupmode' => '1',
                    'availability' => $availability,
                );
            }

            $cmdata = (object) $cmdata;

            $moduleid = $DB->insert_record('course_modules', $cmdata, true, false);

            if ($moduleid) {
                $returnstring .= $moduleid;

                $sequence = $sections->sequence . ',' . $moduleid;

                $sequenceid = $DB->execute('update mdl_course_sections set sequence ="' . $sequence . '" where course=' . $crsid . ' and section=' . $sections->section);

                if ($sequenceid) {

                    if ($grade) {
                        $gidata = array(
                            'itemtype' => 'mod',
                            'itemmodule' => 'quiz',
                            'iteminstance' => $quizid,
                            'idnumber' => $moduleid,
                            'grademax' => $grade,
                            'timecreated' => time(),
                            'timemodified' => time(),
                        );
                        $gidata = (object) $gidata;

                        $DB->insert_record('grade_items', $gidata, true, false);
                    }

                    $DB->execute('update mdl_course set  cacherev =' . time() . ' where id=' . $crsid);

                    $event = \core\event\course_module_created::create(array('courseid' => $crsid, 'context' => context_module::instance($moduleid), 'objectid' => $moduleid, 'other' => array('modulename' => 'quiz', 'name' => $name, 'instanceid' => $quizid)));
                    $event->trigger();

                    return $returnstring;
                }
            }
        }
    }
}

function Save_Assignment($crsid, $name, $desc, $showdesc, $grade, $subdate, $duedate, $topic, $section, $week, $groupId, $fileType, $textType)
{
    global $DB;

    $assigndata = array(
        'course' => $crsid,
        'name' => $name,
        'intro' => $desc,
        'introformat' => '1',
        'alwaysshowdescription' => $showdesc,
        'duedate' => $duedate,
        'allowsubmissionsfromdate' => $subdate,
        'grade' => $grade,
        'timemodified' => time()
    );

    $assigndata = (object) $assigndata;

    $assignid = $DB->insert_record('assign', $assigndata, true, false);

    if ($assignid) {
        $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => 0));

        if ($topic && $topic > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $topic));
        }

        if ($week && $week > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $week));
        }

        if ($section && $section > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $section));
        }

        $cmdata = array(
            'course' => $crsid,
            'module' => '1',
            'instance' => $assignid,
            'section' => $sections->id,
            'added' => time(),
        );

        if ($groupId) {
            $availability = '{\"op\":\"&\",\"c\":[{\"type\":\"group\",\"id\":' . $groupId . '}],\"showc\":[false]}';

            $cmdata = array(
                'course' => $crsid,
                'module' => '1',
                'instance' => $assignid,
                'section' => $sections->id,
                'added' => time(),
                'groupmode' => '1',
                'availability' => $availability,
            );
        }

        $cmdata = (object) $cmdata;

        $moduleid = $DB->insert_record('course_modules', $cmdata, true, false);

        if ($moduleid) {
            $sequence = $sections->sequence . ',' . $moduleid;

            $sequenceid = $DB->execute('update mdl_course_sections set sequence ="' . $sequence . '" where course=' . $crsid . ' and section=' . $sections->section);

            if ($sequenceid) {

                if ($grade) {
                    $record2 = new stdClass();
                    $record2->courseid = $crsid;

                    $hwcategory = $DB->get_record_sql('select gc.* from mdl_grade_items as gi , mdl_grade_categories as gc where gi.idnumber="H_W" and gi.courseid=' . $crsid . ' and gi.iteminstance= gc.id LIMIT 1');

                    if ($hwcategory) {
                        $category = $hwcategory->id;
                    } else {
                        $defaultcategory = $DB->get_record_sql('select gc.* from  mdl_grade_categories as gc where parent is NULL and courseid=' . $crsid);
                        $category = $defaultcategory->id;
                    }

                    $gidata = array(
                        'courseid' => $crsid,
                        'categoryid' => $category,
                        'itemname' => $name,
                        'itemtype' => 'mod',
                        'itemmodule' => 'assign',
                        'iteminstance' => $assignid,
                        'idnumber' => $moduleid,
                        'grademax' => $grade,
                        'timecreated' => time(),
                        'timemodified' => time(),
                    );

                    $gidata = (object) $gidata;

                    $DB->insert_record('grade_items', $gidata, true, false);
                }

                $DB->execute('update mdl_course set  cacherev =' . time() . ' where id=' . $crsid);

                $event = \core\event\course_module_created::create(array('courseid' => $crsid, 'context' => context_module::instance($moduleid), 'objectid' => $moduleid, 'other' => array('modulename' => 'assign', 'name' => $name, 'instanceid' => $assignid)));
                $event->trigger();

                $subType = array('enabled', 'enabled', 'maxfilesubmissions', 'maxsubmissionsizebytes', 'filetypeslist');
                $value = array($textType, $fileType, 1, 0, ' ');

                for ($count = 0; $count < 5; $count++) {
                    $plugin = "file";

                    if ($count == 1) $plugin = "onlineText";

                    $mapcdata = array(
                        'assignment' => $assignid,
                        'plugin' => $plugin,
                        'subtype' => 'assignsubmission',
                        'name' => $subType[$count],
                        'value' => $value[$count],
                    );

                    $mapcdata = (object) $mapcdata;

                    $DB->insert_record('assign_plugin_config', $mapcdata, true, false);
                }

                return $moduleid;
            }
        }
    }
}

function Save_Label($crsid, $desc, $topic, $section, $week, $groupId)
{
    global $DB;

    $labeldata = array(
        'course' => $crsid,
        'name' => 'name',
        'intro' => $desc,
        'introformat' => "1",
        'timemodified' => time(),
    );
    $labeldata = (object) $labeldata;

    $labelid = $DB->insert_record('label', $labeldata, true, false);

    if ($labelid) {
        $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => 0));

        if ($topic && $topic > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $topic));
        }

        if ($week && $week > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $week));
        }

        if ($section && $section > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $section));
        }

        $cmdata = array(
            'course' => $crsid,
            'module' => '12',
            'instance' => $labelid,
            'section' => $sections->id,
            'added' => time(),
        );

        if ($groupId) {
            $availability = '{\"op\":\"&\",\"c\":[{\"type\":\"group\",\"id\":' . $groupId . '}],\"showc\":[false]}';

            $cmdata = array(
                'course' => $crsid,
                'module' => '12',
                'instance' => $labelid,
                'section' => $sections->id,
                'added' => time(),
                'groupmode' => '1',
                'availability' => $availability,
            );
        }

        $cmdata = (object) $cmdata;

        $moduleid = $DB->insert_record('course_modules', $cmdata, true, false);

        if ($moduleid) {
            $sequence = $sections->sequence . ',' . $moduleid;

            $DB->execute('update mdl_course_sections set sequence ="' . $sequence . '" where course=' . $crsid . ' and section=' . $sections->section);

            $DB->execute('update mdl_course set  cacherev =' . time() . ' where id=' . $crsid);

            $event = \core\event\course_module_created::create(array('courseid' => $crsid, 'context' => context_module::instance($moduleid), 'objectid' => $moduleid, 'other' => array('modulename' => 'label', 'name' => 'label', 'instanceid' => $labelid)));
            $event->trigger();

            return $labelid . ',' . $moduleid;
        }
    }
}

function Save_URL($crsid, $name, $url, $desc, $topic, $section, $week, $groupId)
{
    global $DB;

    $urldata = array(
        'course' => $crsid,
        'name' => 'name',
        'intro' => $desc,
        'introformat' => "1",
        'timemodified' => time(),
        'externalurl' => $url
    );
    $urldata = (object) $urldata;

    $urlid = $DB->insert_record('url', $urldata, true, false);

    if ($urlid) {
        $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => 0));

        if ($topic && $topic > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $topic));
        }

        if ($week && $week > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $week));
        }

        if ($section && $section > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $section));
        }

        $cmdata = array(
            'course' => $crsid,
            'module' => '20',
            'instance' => $urlid,
            'section' => $sections->id,
            'added' => time(),
        );

        if ($groupId) {
            $availability = '{\"op\":\"&\",\"c\":[{\"type\":\"group\",\"id\":' . $groupId . '}],\"showc\":[false]}';

            $cmdata = array(
                'course' => $crsid,
                'module' => '20',
                'instance' => $urlid,
                'section' => $sections->id,
                'added' => time(),
                'groupmode' => '1',
                'availability' => $availability,
            );
        }

        $cmdata = (object) $cmdata;

        $moduleid = $DB->insert_record('course_modules', $cmdata, true, false);

        if ($moduleid) {
            $sequence = $sections->sequence . ',' . $moduleid;

            $DB->execute('update mdl_course_sections set sequence ="' . $sequence . '" where course=' . $crsid . ' and section=' . $sections->section);

            $DB->execute('update mdl_course set  cacherev =' . time() . ' where id=' . $crsid);

            $event = \core\event\course_module_created::create(array('courseid' => $crsid, 'context' => context_module::instance($moduleid), 'objectid' => $moduleid, 'other' => array('modulename' => 'url', 'name' => $name, 'instanceid' => $urlid)));
            $event->trigger();

            return $moduleid;
        }
    }
}

function Save_Resource($crsid, $name, $desc, $topic, $section, $week, $groupId)
{
    global $DB;

    $resourcedata = array(
        'course' => $crsid,
        'name' => $name,
        'intro' => $desc,
        'introformat' => "1",
        'timemodified' => time()
    );
    $resourcedata = (object) $resourcedata;

    $resourceid = $DB->insert_record('resource', $resourcedata, true, false);

    if ($resourceid) {
        $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => 0));

        if ($topic && $topic > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $topic));
        }

        if ($week && $week > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $week));
        }

        if ($section && $section > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $section));
        }

        $cmdata = array(
            'course' => $crsid,
            'module' => '17',
            'instance' => $resourceid,
            'section' => $sections->id,
            'added' => time(),
        );

        if ($groupId) {
            $availability = '{\"op\":\"&\",\"c\":[{\"type\":\"group\",\"id\":' . $groupId . '}],\"showc\":[false]}';

            $cmdata = array(
                'course' => $crsid,
                'module' => '17',
                'instance' => $resourceid,
                'section' => $sections->id,
                'added' => time(),
                'groupmode' => '1',
                'availability' => $availability,
            );
        }

        $cmdata = (object) $cmdata;

        $moduleid = $DB->insert_record('course_modules', $cmdata, true, false);

        if ($moduleid) {
            $sequence = $sections->sequence . ',' . $moduleid;

            $DB->execute('update mdl_course_sections set sequence ="' . $sequence . '" where course=' . $crsid . ' and section=' . $sections->section);

            $DB->execute('update mdl_course set  cacherev =' . time() . ' where id=' . $crsid);

            $event = \core\event\course_module_created::create(array('courseid' => $crsid, 'context' => context_module::instance($moduleid), 'objectid' => $moduleid, 'other' => array('modulename' => 'resource', 'name' => $name, 'instanceid' => $resourceid)));
            $event->trigger();

            return $moduleid;
        }
    }
}

function Save_Folder($crsid, $name, $desc, $topic, $section, $week, $groupId)
{
    global $DB;

    $folderdata = array(
        'course' => $crsid,
        'name' => $name,
        'intro' => $desc,
        'introformat' => "1",
        'timemodified' => time(),
        'display' => '1'
    );
    $folderdata = (object) $folderdata;

    $folderid = $DB->insert_record('folder', $folderdata, true, false);

    if ($folderid) {
        $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => 0));

        if ($topic && $topic > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $topic));
        }

        if ($week && $week > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $week));
        }

        if ($section && $section > 0) {
            $sections = $DB->get_record('course_sections', array('course' => $crsid, 'section' => $section));
        }

        $cmdata = array(
            'course' => $crsid,
            'module' => '8',
            'instance' => $folderid,
            'section' => $sections->id,
            'added' => time(),
        );

        if ($groupId) {
            $availability = '{\"op\":\"&\",\"c\":[{\"type\":\"group\",\"id\":' . $groupId . '}],\"showc\":[false]}';

            $cmdata = array(
                'course' => $crsid,
                'module' => '8',
                'instance' => $folderid,
                'section' => $sections->id,
                'added' => time(),
                'groupmode' => '1',
                'availability' => $availability,
            );
        }

        $cmdata = (object) $cmdata;

        $moduleid = $DB->insert_record('course_modules', $cmdata, true, false);

        if ($moduleid) {
            $sequence = $sections->sequence . ',' . $moduleid;

            $DB->execute('update mdl_course_sections set sequence ="' . $sequence . '" where course=' . $crsid . ' and section=' . $sections->section);

            $DB->execute('update mdl_course set  cacherev =' . time() . ' where id=' . $crsid);

            $event = \core\event\course_module_created::create(array('courseid' => $crsid, 'context' => context_module::instance($moduleid), 'objectid' => $moduleid, 'other' => array('modulename' => 'folder', 'name' => $name, 'instanceid' => $folderid)));
            $event->trigger();

            return $moduleid;
        }
    }
}

function Get_Students_Access_Codes_By_Id($studentsIds)
{
    global $DB;

    $accesscodes = array();
    $i = 0;

    foreach ($studentsIds as $id) {
        $accesscode = $DB->get_record_sql('select ud.data from mdl_user_info_field as uf , mdl_user_info_data as ud where uf.shortname="AccessCode" and ud.fieldid=uf.id and ud.userid=? limit 1', array($id));

        if ($accesscode) {
            $accesscodes[$i] = $accesscode->data;
            $i++;
        }
    }
    return $accesscodes;
}
